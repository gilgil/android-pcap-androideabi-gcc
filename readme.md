Build pcap library for android with arm-linux-androideabi-gcc
=============================================================

* Download Android NDK(suppose that extract folder is /root/android/ndk)  
`
http://developer.android.com/ndk
`

* Install compile tools  
`
sudo apt install byacc  
sudo apt install flex
`

* Download libpcap source cde and extract it  
`
wget http://www.tcpdump.org/release/libpcap-1.8.1.tar.gz  
tar xf libpcap-1.8.1.tar.gz  
cd libpcap-1.8.1
`

* Export environment variables  
`
export ANDROID_NDK_ROOT=/root/android/ndk  
export CC=$ANDROID_NDK_ROOT/toolchains/arm-linux-androideabi-4.9/prebuilt/linux-x86_64/bin/arm-linux-androideabi-gcc  
export CFLAGS=-I$ANDROID_NDK_ROOT/platforms/android-16/arch-arm/usr/include  
export LDFLAGS=--sysroot=$ANDROID_NDK_ROOT/platforms/android-16/arch-arm
`

* Excute the configure file  
`
./configure --host=arm-linux --with-pcap=linux --prefix=$PWD/../usr
`

* Execute "make" to build pcap library  
`
make
`

* Execute "make install" and you can see final output files in "final" directory.  
`
make install
`

references  
https://www.androidtcpdump.com/android-tcpdump/compile  
https://youtu.be/oE3hwmQJNM0
